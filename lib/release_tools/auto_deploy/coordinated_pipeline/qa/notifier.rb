# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Qa
        # Notifier sends notifications for failed downstream QA pipelines.
        class Notifier
          include ReleaseTools::AutoDeploy::Pipeline
          include ::SemanticLogger::Loggable

          def initialize(pipeline_id:, deploy_version:, environment:)
            @pipeline_id    = pipeline_id
            @deploy_version = deploy_version
            @environment    = environment
          end

          def execute
            qa_pipeline = find_downstream_pipeline

            if qa_pipeline
              logger.info('QA pipeline found', quality_pipeline: qa_pipeline.web_url)

              send_slack_notification(qa_pipeline)
            else
              logger.fatal('QA pipeline not found')
            end
          end

          private

          attr_reader :pipeline_id, :deploy_version, :environment

          def find_downstream_pipeline
            logger.info('Fetching qa downstream pipeline', environment: environment, deploy_version: deploy_version, pipeline_id: pipeline_id)

            bridges
              .find { |job| job.name == "qa:smoke:#{environment}" && job.status == 'failed' }
              &.downstream_pipeline
          rescue MissingPipelineError
            nil
          end

          def bridges
            pipeline_bridges = Retriable.with_context(:pipeline_created) do
              logger.debug('Finding QA bridges for pipeline', pipeline_id: pipeline_id)

              ReleaseTools::GitlabOpsClient.pipeline_bridges(Project::ReleaseTools, pipeline_id)
            end

            raise MissingPipelineError if pipeline_bridges.empty?

            pipeline_bridges
          end

          def send_slack_notification(qa_pipeline)
            options = {
              deploy_version: deploy_version,
              pipeline: qa_pipeline,
              environment: environment
            }

            ReleaseTools::Slack::QaNotification.new(options).execute
          end
        end
      end
    end
  end
end

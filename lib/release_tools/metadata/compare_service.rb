# frozen_string_literal: true

module ReleaseTools
  module Metadata
    # Generates a `Metadata::Comparison` between a source version and a previous version.
    class CompareService
      include ::SemanticLogger::Loggable

      # @param source [ProductVersion] version that needs to be compared.
      # @param environment [String] where source version is deployed.
      def initialize(source:, environment:)
        @source = source
        @environment = environment
      end

      def with_latest_successful_deployment
        target_version = ProductVersion.from_metadata_sha(latest_successful_deployment.sha)

        if target_version > source
          Comparison.new(source: target_version, target: source)
        else
          Comparison.new(source: source, target: target_version)
        end
      end

      private

      attr_reader :source, :environment

      def latest_successful_deployment
        Retriable.with_context(:api) do
          GitlabOpsClient.deployments(
            Project::Release::Metadata,
            environment,
            status: 'success',
            order_by: 'id',
            sort: 'desc',
            opts: {
              per_page: 1
            }
          ).first
        end
      end
    end
  end
end

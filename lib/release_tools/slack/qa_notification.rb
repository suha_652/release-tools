# frozen_string_literal: true

module ReleaseTools
  module Slack
    class QaNotification
      include ::SemanticLogger::Loggable
      include Utilities

      def initialize(deploy_version:, pipeline:, environment:)
        @deploy_version = deploy_version
        @pipeline = pipeline
        @environment = environment
      end

      def execute
        logger.info('Sending qa slack notification', deploy_version: deploy_version, environment: environment, quality_url: pipeline.web_url, slack_channel: slack_channel)

        return if SharedStatus.dry_run?

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: fallback_text,
          blocks: slack_block
        )
      end

      private

      attr_reader :deploy_version, :pipeline, :environment, :slack_channel

      def fallback_text
        "qa #{environment} failed #{deploy_version}"
      end

      def slack_block
        [
          {
            type: 'section',
            text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
          },
          {
            type: 'context',
            elements: [clock_context_element]
          }
        ]
      end

      def section_block
        [].tap do |text|
          text << environment_icon
          text << status_icon
          text << "*QA #{environment}*"
          text << "<#{pipeline.web_url}|failed>"
          text << "`#{deploy_version}`"
        end.join(' ')
      end
    end
  end
end

# frozen_string_literal: true

module ReleaseTools
  module Project
    module Quality
      class Production < Base
        REMOTES = {
          canonical: 'git@gitlab.com:gitlab-org/quality/production.git',
          ops: 'git@ops.gitlab.net:gitlab-org/quality/production.git'
        }.freeze

        def self.environment
          'gprd'
        end
      end
    end
  end
end

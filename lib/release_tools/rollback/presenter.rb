# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Presents a Validator instance in GitLab Markdown or Slack format
    class Presenter
      include ::SemanticLogger::Loggable

      LINK_FORMAT = 'https://gitlab.com/gitlab-org/security/gitlab/-/blob/%<commit>s/%<file>s'

      Link = Struct.new(:text, :url) do
        def to_s
          to_markdown
        end

        def to_markdown
          "* [`#{text}`](#{url})"
        end

        def to_slack
          "• <#{url}|`#{text}`>"
        end
      end

      # comparison - Rollback::Comparison instance
      # deployment - Rollback::UpcomingDeployments instance
      def initialize(comparison, upcoming_deployment)
        @comparison = comparison
        @upcoming_deployment = upcoming_deployment
      end

      def header
        if safe_to_rollback?
          ":large_green_circle: Rollback available"
        else
          ":red_circle: Rollback unavailable #{rollback_unavailable_header}"
        end
      end

      def present
        lines = [
          "*Current:* `#{@comparison.current_auto_deploy_package}`\n",
          "*Target:* `#{@comparison.target_auto_deploy_package}`\n"
        ]

        lines << "*Comparison:* #{@comparison.web_url}\n"
        lines << ":warning: Comparison timed out\n" if @comparison.timeout?
        lines << ":warning: Comparison was empty\n" if @comparison.empty?

        lines.push(*post_deploy_status)

        lines
      end

      def unavailable_rollback_multireason?
        rollback_unavailable_reasons.size > 1
      end

      def rollback_unavailable_reasons_block
        rollback_unavailable_reasons.join("\n")
      end

      private

      def blob_url(file)
        format(LINK_FORMAT, commit: @comparison.current_rails_sha, file: file)
      end

      def safe_to_rollback?
        @comparison.safe? && !deployment_in_progress?
      end

      def post_migrations_included?
        @comparison.post_deploy_migrations.any?
      end

      def deployment_in_progress?
        @upcoming_deployment.any?
      end

      def rollback_unavailable_header
        return unless rollback_unavailable_reasons.size == 1

        rollback_unavailable_reasons.first
      end

      def rollback_unavailable_reasons
        @rollback_unavailable_reasons ||= begin
          [].tap do |reasons|
            reasons << "- A deployment is in progress" if deployment_in_progress?
            reasons << "- Post-deployment migrations included" if post_migrations_included?
          end
        end
      end

      def post_deploy_status
        return if @comparison.post_deploy_migrations.none?

        status = [":new: #{@comparison.post_deploy_migrations.size} post-deploy migrations\n"]

        @comparison.post_deploy_migrations.each do |migration|
          text = migration['new_path'].delete_prefix('db/post_migrate/')
          status << Link.new(text, blob_url(migration['new_path']))
        end

        status
      end
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::AutoDeploy::RollbackCheck do
  include RollbackHelper

  let(:fake_compare_service) { double(:compare_service) }
  let(:current_rails_sha) { 'bf7bc31ffb1' }
  let(:web_url) { 'https://test.gitlab.com/gitlab/-/compare/bf7bc31ffb1...10438b18493' }

  let(:post_deploy_migration) do
    {
      'new_path' => 'db/post_migrate/20211030_new_post_deploy_migration.rb'
    }
  end

  subject(:service) { described_class.new }

  def stub_comparison(safe: true, post_deploy_migrations: [])
    attributes = {
      current: stub_product_version('bf7bc31ffb1', '2c2465c4dca'),
      target: stub_product_version('10438b18493', '6793946906e'),
      web_url: web_url,
      current_rails_sha: current_rails_sha,
      safe?: safe,
      post_deploy_migrations: post_deploy_migrations
    }

    rollback_stub_comparison(attributes)
  end

  def stub_auto_deploy_version(package)
    ReleaseTools::AutoDeploy::Version
      .new(package)
  end

  def stub_main_reason_block(reason)
    {
      text: {
        emoji: true,
        text: reason,
        type: 'plain_text'
      },
      type: 'header'
    }
  end

  def stub_divider
    {
      type: 'divider'
    }
  end

  def stub_rollback_unavailable_reasons_block
    {
      text: {
        text: "- A deployment is in progress\n- Post-deployment migrations included",
        type: 'mrkdwn'
      },
      type: 'section'
    }
  end

  def stub_comparison_block(include_post_deploy_migration: false)
    comparison_text = <<~HEREDOC
      *Current:* `42.1.2021110116-bf7bc31ffb1.2c2465c4dca`

      *Target:* `42.1.2021110116-10438b18493.6793946906e`

      *Comparison:* https://test.gitlab.com/gitlab/-/compare/bf7bc31ffb1...10438b18493
    HEREDOC

    post_migration_section = <<~HEREDOC.strip

      :new: 1 post-deploy migrations

      • <https://gitlab.com/gitlab-org/security/gitlab/-/blob/bf7bc31ffb1/db/post_migrate/20211030_new_post_deploy_migration.rb|`20211030_new_post_deploy_migration.rb`>
    HEREDOC

    current_text = if include_post_deploy_migration
                     <<~HEREDOC.strip
                       #{comparison_text}
                       #{post_migration_section}
                     HEREDOC
                   else
                     comparison_text
                   end

    {
      text: {
        text: current_text,
        type: 'mrkdwn'
      },
      type: 'section'
    }
  end

  def stub_elements_block
    {
      elements: [
        {
          text: ":book: <https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/rollback-a-deployment.md|View runbook>",
          type: 'mrkdwn'
        }
      ],
      type: 'context'
    }
  end

  describe '#execute' do
    around do |ex|
      ClimateControl.modify(
        SLACK_TOKEN: '123456',
        ROLLBACK_CURRENT: 'gprd',
        ROLLBACK_TARGET: 'gprd',
        &ex
      )
    end

    before do
      allow(ReleaseTools::Rollback::CompareService)
        .to receive(:new)
        .and_return(fake_compare_service)

      allow(ReleaseTools::Rollback::UpcomingDeployments)
        .to receive(:new)
        .and_return(rollback_upcoming_deployment_stub)

      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
    end

    context 'when a rollback is available' do
      before do
        allow(fake_compare_service)
          .to receive(:execute)
          .and_return(stub_comparison)
      end

      it "states it's safe to rollback" do
        blocks = [
          stub_main_reason_block(':large_green_circle: Rollback available'),
          stub_divider,
          stub_comparison_block,
          stub_elements_block
        ]

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            message: 'Rollback check results',
            blocks: blocks
          )

        service.execute
      end
    end

    context 'when a rollback is not available' do
      context 'with post-deployment migrations' do
        it "states it's not safe to rollback" do
          comparison = stub_comparison(safe: false, post_deploy_migrations: [post_deploy_migration])

          allow(fake_compare_service)
            .to receive(:execute)
            .and_return(comparison)

          blocks = [
            stub_main_reason_block(':red_circle: Rollback unavailable - Post-deployment migrations included'),
            stub_divider,
            stub_comparison_block(include_post_deploy_migration: true),
            stub_elements_block
          ]

          expect(ReleaseTools::Slack::Message)
            .to receive(:post)
            .with(
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              message: 'Rollback check results',
              blocks: blocks
            )

          service.execute
        end
      end

      context 'with a deployment in progress' do
        it "states it's not safe to rollback" do
          allow(fake_compare_service)
            .to receive(:execute)
            .and_return(stub_comparison)

          allow(ReleaseTools::Rollback::UpcomingDeployments)
            .to receive(:new)
            .and_return(rollback_upcoming_deployment_stub(any?: true))

          blocks = [
            stub_main_reason_block(':red_circle: Rollback unavailable - A deployment is in progress'),
            stub_divider,
            stub_comparison_block,
            stub_elements_block
          ]

          expect(ReleaseTools::Slack::Message)
            .to receive(:post)
            .with(
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              message: 'Rollback check results',
              blocks: blocks
            )

          service.execute
        end
      end

      context 'with a deployment in progress and with post-migrations included' do
        it "states it's not safe to rollback" do
          allow(fake_compare_service)
            .to receive(:execute)
            .and_return(stub_comparison(safe: false, post_deploy_migrations: [post_deploy_migration]))

          allow(ReleaseTools::Rollback::UpcomingDeployments)
            .to receive(:new)
            .and_return(rollback_upcoming_deployment_stub(any?: true))

          blocks = [
            stub_main_reason_block(':red_circle: Rollback unavailable '),
            stub_rollback_unavailable_reasons_block,
            stub_divider,
            stub_comparison_block(include_post_deploy_migration: true),
            stub_elements_block
          ]

          expect(ReleaseTools::Slack::Message)
            .to receive(:post)
            .with(
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              message: 'Rollback check results',
              blocks: blocks
            )

          service.execute
        end
      end
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::ProductionCheck::Incident do
  let!(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:project) { 'group/project' }

  around do |ex|
    ClimateControl.modify(
      GITLAB_PRODUCTION_PROJECT_PATH: project,
      PRODUCTION_ISSUE_IID: '1234',
      &ex
    )
  end

  describe '#execute' do
    it 'finds a specified issue and adds a comment with the current status' do
      instance = described_class.new
      status = instance_double(
        'ReleaseTools::Promotion::ProductionStatus',
        to_issue_body: 'Issue body'
      )
      issue = instance_double('Gitlab::ObjectifiedHash')

      expect(fake_client).to receive(:issue)
        .with(project, '1234')
        .and_return(issue)
      stub_const('ReleaseTools::Promotion::ProductionStatus', double(new: status))

      without_dry_run do
        instance.execute
      end

      expect(fake_client).to have_received(:create_issue_note)
        .with(project, issue: issue, body: 'Issue body')
    end
  end
end

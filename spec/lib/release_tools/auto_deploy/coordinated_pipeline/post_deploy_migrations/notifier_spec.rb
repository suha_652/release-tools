# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::PostDeployMigrations::Notifier do
  let(:fake_client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  let(:fake_notification) do
    stub_const('ReleaseTools::Slack::PostDeployMigrationsNotification', spy)
  end

  subject(:notifier) do
    described_class.new(
      pipeline_id: '123',
      deploy_version: '14.6.202112160920-a7ab264e1a4.6ab0d28e374',
      environment: 'gstg'
    )
  end

  describe '#execute' do
    context 'when post-deploy migrations pipeline can be found' do
      let(:post_deploy_migrations_pipeline) do
        create(
          :pipeline,
          :success,
          web_url: 'https://test.gitlab.net/deployer/-/pipelines/123'
        )
      end

      let(:post_deploy_migrations_bridge) do
        create(
          :gitlab_response,
          name: 'postdeploy-migrations:gstg',
          downstream_pipeline: post_deploy_migrations_pipeline
        )
      end

      it 'sends a slack notification' do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([post_deploy_migrations_bridge])

        allow(fake_notification)
          .to receive(:new)
          .and_return(double(execute: nil))

        expect(ReleaseTools::GitlabOpsClient)
          .to receive(:pipeline_bridges)

        expect(ReleaseTools::Slack::PostDeployMigrationsNotification)
          .to receive(:new)
          .with(
            deploy_version: '14.6.202112160920-a7ab264e1a4.6ab0d28e374',
            pipeline: post_deploy_migrations_pipeline,
            environment: 'gstg'
          )

        notifier.execute
      end
    end

    context 'when the bridge job can not be found' do
      it 'does nothing' do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([])

        expect(ReleaseTools::GitlabOpsClient)
          .to receive(:pipeline_bridges)
          .at_least(:twice)

        expect(ReleaseTools::Slack::PostDeployMigrationsNotification)
          .not_to receive(:new)

        notifier.execute
      end
    end

    context 'when the downstream pipeline can not be found' do
      let(:post_deploy_migrations_bridge) do
        create(
          :gitlab_response,
          name: 'postdeploy-migrations:gstg',
          downstream_pipeline: nil
        )
      end

      it 'does nothing' do
        allow(fake_client)
          .to receive(:pipeline_bridges)
          .and_return([post_deploy_migrations_bridge])

        expect(ReleaseTools::GitlabOpsClient)
          .to receive(:pipeline_bridges)
          .at_least(:twice)

        expect(ReleaseTools::Slack::PostDeployMigrationsNotification)
          .not_to receive(:new)

        notifier.execute
      end
    end
  end
end

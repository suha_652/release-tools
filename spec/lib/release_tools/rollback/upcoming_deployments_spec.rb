# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::UpcomingDeployments do
  let!(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:environment) { 'gprd' }

  subject(:upcoming_deployments) do
    described_class.new(environment: environment)
  end

  describe '#any?' do
    let(:fake_response) { double(:response) }

    before do
      allow(fake_client)
        .to receive(:deployments)
        .and_return(fake_response)
    end

    context 'with a running deployment' do
      let(:deployment) { create(:deployment, :running) }

      before do
        allow(fake_response)
          .to receive(:paginate_with_limit)
          .and_return([deployment])
      end

      it 'returns true' do
        expect(upcoming_deployments).to be_any
      end
    end

    context 'with no running deployment' do
      it 'returns false' do
        allow(fake_response)
          .to receive(:paginate_with_limit)
          .and_return([])

        expect(upcoming_deployments).not_to be_any
      end
    end

    context 'with an invalid environment' do
      let(:environment) { 'foo' }

      it 'returns false' do
        expect(upcoming_deployments).not_to be_any
      end
    end
  end

  describe '#stale_cleanup' do
    context "with `cleanup_stale_deployments` disabled" do
      it "does nothing" do
        without_dry_run do
          expect(upcoming_deployments.stale_cleanup).to eq(nil)
        end
      end
    end

    context "with `cleanup_stale_deployments` enabled" do
      before do
        enable_feature(:cleanup_stale_deployments)
      end

      it "marks stale deployments as failed" do
        recent = create(:deployment, :running)
        stale = create(:deployment, :running, created_at: 30.days.ago.utc.iso8601)

        allow(fake_client).to receive(:deployments)
          .and_return(Gitlab::PaginatedResponse.new([recent, stale]))

        expect(fake_client).to receive(:update_deployment)
          .with(anything, stale.id, status: 'failed')
        expect(fake_client).not_to receive(:update_deployment)
          .with(anything, recent.id, anything)

        without_dry_run do
          upcoming_deployments.stale_cleanup
        end
      end
    end
  end
end

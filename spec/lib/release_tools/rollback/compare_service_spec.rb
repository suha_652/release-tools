# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::CompareService do
  let!(:fake_client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  def stub_deployments(deployments, environment = 'fake-production')
    allow(fake_client).to receive(:deployments)
      .with(described_class::PROJECT, environment)
      .and_return(deployments)
  end

  def stub_product_versions_fetch_and_comparison(current_version, target_version)
    [current_version, target_version].each do |product_version|
      normalized_version = "#{product_version.major}.#{product_version.minor}.#{product_version.patch}"
      meta_path = "releases/#{product_version.major}/#{normalized_version}.json"

      allow(fake_client).to receive(:commit_diff)
        .with(described_class::PROJECT, product_version.metadata_commit_id)
        .and_return([create(:diff, new_path: meta_path, new_file: true)])

      allow(ReleaseTools::ProductVersion).to receive(:from_package_version)
        .with(normalized_version)
        .and_return(product_version)
    end

    expect(ReleaseTools::Rollback::Comparison).to receive(:new)
      .with(current: current_version, target: target_version)
      .and_return(double('comparison', execute: nil))
  end

  describe 'initialize' do
    it 'attempts to parse from packages' do
      omnibus_package = '42.1.2021121314+f7e5665fa11.3fb2052b8b6'
      product_version = '42.1.2021121314'

      instance = described_class.new(current: omnibus_package, target: omnibus_package)

      expect(instance.current.to_s).to eq(product_version)
      expect(instance.target.to_s).to eq(product_version)
    end
  end

  describe '#execute' do
    it 'fetches from environment' do
      deployments = [
        build(:deployment, :failed),
        build(:deployment, :success),
        build(:deployment, :failed),
        build(:deployment, :success)
      ]
      failed_deployment = deployments[0]
      success_deployment = deployments[1]
      failed_product_version = build(:product_version, metadata_commit_id: failed_deployment.sha)
      success_product_version = build(:product_version, metadata_commit_id: success_deployment.sha)

      stub_deployments(deployments, 'gprd')
      stub_product_versions_fetch_and_comparison(failed_product_version, success_product_version)

      instance = described_class.new(current: 'gprd', target: 'gprd')
      instance.execute

      expect(instance.current.metadata_commit_id).to eq(failed_deployment.sha)
      expect(instance.target.metadata_commit_id).to eq(success_deployment.sha)
    end

    it 'running deployments are not considered as success for target deployment' do
      deployments = [
        build(:deployment, :failed),
        build(:deployment, :running),
        build(:deployment, :failed),
        build(:deployment, :success)
      ]
      failed_deployment = deployments[0]
      success_deployment = deployments[-1]
      failed_product_version = build(:product_version, metadata_commit_id: failed_deployment.sha)
      success_product_version = build(:product_version, metadata_commit_id: success_deployment.sha)

      stub_deployments(deployments, 'gprd')
      stub_product_versions_fetch_and_comparison(failed_product_version, success_product_version)

      instance = described_class.new(current: 'gprd', target: 'gprd')
      instance.execute

      expect(instance.current.metadata_commit_id).to eq(failed_deployment.sha)
      expect(instance.target.metadata_commit_id).to eq(success_deployment.sha)
    end

    it 'compares a current successful deployment to the previous successful deployment' do
      deployments = [
        build(:deployment, :success),
        build(:deployment, :failed),
        build(:deployment, :success),
        build(:deployment, :failed)
      ]
      current_deployment = deployments[0]
      previous_deployment = deployments[2]
      current_product_version = build(:product_version, metadata_commit_id: current_deployment.sha)
      previous_product_version = build(:product_version, metadata_commit_id: previous_deployment.sha)

      stub_deployments(deployments, 'gprd')
      stub_product_versions_fetch_and_comparison(current_product_version, previous_product_version)

      instance = described_class.new(current: 'gprd', target: 'gprd')
      instance.execute

      expect(instance.current.metadata_commit_id).to eq(current_deployment.sha)
      expect(instance.target.metadata_commit_id).to eq(previous_deployment.sha)
    end

    it 'compares a package to the previous successful deployment' do
      deployments = [
        build(:deployment, :success),
        build(:deployment, :success)
      ]
      current_deployment = deployments[0]
      previous_deployment = deployments[-1]
      current_product_version = build(:product_version, metadata_commit_id: current_deployment.sha)
      previous_product_version = build(:product_version, metadata_commit_id: previous_deployment.sha)
      current_package = current_product_version.to_s

      allow(ReleaseTools::ProductVersion).to receive(:from_auto_deploy)
        .with(current_package)
        .and_return(current_product_version)

      stub_deployments(deployments, 'gprd')
      stub_product_versions_fetch_and_comparison(current_product_version, previous_product_version)

      instance = described_class.new(current: current_product_version.to_s, target: 'gprd')
      instance.execute

      expect(instance.current.metadata_commit_id).to eq(current_deployment.sha)
      expect(instance.target.metadata_commit_id).to eq(previous_deployment.sha)
    end

    it "raises ArgumentError if deployments can't be retrieved" do
      deployments = [
        build(:deployment, :failed),
        build(:deployment, :failed)
      ]
      failed_deployment = deployments[0]
      failed_product_version = build(:product_version, metadata_commit_id: failed_deployment.sha)

      stub_deployments(deployments, 'gprd')

      expect(ReleaseTools::Rollback::Comparison).not_to receive(:new)

      instance = described_class.new(current: 'gprd', target: 'gprd')
      expect(instance).to receive(:from_metadata_commit_id)
                            .with(failed_deployment.sha)
                            .and_return(failed_product_version)
      expect(instance).to receive(:from_metadata_commit_id)
                            .with(nil)

      expect { instance.execute }.to raise_error(ArgumentError)
    end
  end
end

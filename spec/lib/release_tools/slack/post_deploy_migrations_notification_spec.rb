# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::PostDeployMigrationsNotification do
  let(:deploy_version) { '14.6.202112160920-a7ab264e1a4.6ab0d28e374' }
  let(:environment) { 'gstg' }

  let(:post_migration_pipeline) do
    create(
      :pipeline,
      :success,
      web_url: 'https://test.com/quality/-/pipelines/123'
    )
  end

  subject(:notifier) do
    described_class.new(
      deploy_version: deploy_version,
      pipeline: post_migration_pipeline,
      environment: environment
    )
  end

  describe '#execute' do
    before do
      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
        .and_return({})
    end

    context 'when post-deploy migrations succeed' do
      it 'sends a slack message' do
        block_content =
          ":building_construction: :ci_passing: *Post-deploy migrations gstg* <https://test.com/quality/-/pipelines/123|finished> `#{deploy_version}`"

        context_elements = [
          { text: ':clock1: 2021-09-14 15:55 UTC', type: 'mrkdwn' }
        ]

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: 'post-deploy migrations gstg finished 14.6.202112160920-a7ab264e1a4.6ab0d28e374',
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
            notifier.execute
          end
        end
      end
    end

    context 'when post-deploy migrations fail' do
      let(:post_migration_pipeline) do
        create(
          :pipeline,
          :failed,
          web_url: 'https://test.com/quality/-/pipelines/123'
        )
      end

      it 'sends a slack message' do
        block_content =
          ":building_construction: :ci_failing: *Post-deploy migrations gstg* <https://test.com/quality/-/pipelines/123|failed> `#{deploy_version}`"

        context_elements = [
          { text: ':clock1: 2021-09-14 15:55 UTC', type: 'mrkdwn' }
        ]

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: 'post-deploy migrations gstg failed 14.6.202112160920-a7ab264e1a4.6ab0d28e374',
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
            notifier.execute
          end
        end
      end
    end

    context 'when post-deploy migrations start' do
      let(:post_migration_pipeline) do
        create(
          :pipeline,
          :running,
          web_url: 'https://test.com/quality/-/pipelines/123'
        )
      end

      it 'sends a slack message' do
        block_content =
          ":building_construction: :ci_running: *Post-deploy migrations gstg* <https://test.com/quality/-/pipelines/123|started> `#{deploy_version}`"

        context_elements = [
          { text: ':clock1: 2021-09-14 15:55 UTC', type: 'mrkdwn' }
        ]

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: 'post-deploy migrations gstg started 14.6.202112160920-a7ab264e1a4.6ab0d28e374',
            blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
          )

        without_dry_run do
          Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
            notifier.execute
          end
        end
      end
    end

    context 'with a dry-run' do
      it 'does nothing' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        notifier.execute
      end
    end
  end
end

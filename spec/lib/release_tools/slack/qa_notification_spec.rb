# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::QaNotification do
  let(:deploy_version) { '14.6.202112031820-3605bffdaf6.c516dd235a1' }
  let(:environment) { 'gstg' }

  let(:quality_pipeline) do
    create(
      :pipeline,
      :failed,
      web_url: 'https://test.com/quality/-/pipelines/123'
    )
  end

  subject(:notifier) do
    described_class.new(
      deploy_version: deploy_version,
      pipeline: quality_pipeline,
      environment: environment
    )
  end

  describe '#execute' do
    before do
      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
        .and_return({})
    end

    it 'sends a slack message' do
      block_content =
        ":building_construction: :ci_failing: *QA gstg* <https://test.com/quality/-/pipelines/123|failed> `#{deploy_version}`"

      context_elements = [
        { text: ':clock1: 2021-09-14 15:55 UTC', type: 'mrkdwn' }
      ]

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: "qa gstg failed 14.6.202112031820-3605bffdaf6.c516dd235a1",
          blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
        )

      without_dry_run do
        Timecop.freeze(Time.utc(2021, 9, 14, 15, 55, 0)) do
          notifier.execute
        end
      end
    end

    context 'with dry-run' do
      it 'does nothing' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        notifier.execute
      end
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::CoordinatedPipelineTagNotification do
  let(:deploy_version) { '14.0.202105311735+ac7ff9aa2f2.2ab4d081326' }
  let(:auto_deploy_branch) { '42-1-auto-deploy-2021010200' }
  let(:tag_version) { '14.4.202110082020' }

  subject(:tag_notifier) do
    described_class.new(
      deploy_version: deploy_version,
      auto_deploy_branch: auto_deploy_branch,
      tag_version: tag_version
    )
  end

  describe '#execute' do
    it 'posts notification to Slack' do
      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          message: %r{New coordinated pipeline: <https://test.net/gitlab-org/release/tools/-/pipelines/123|14.4.202110082020> (<https://gitlab.com/gitlab-org/security/gitlab/-/commits/42-1-auto-deploy-2021010200|42-1-auto-deploy-2021010200> @ `ac7ff9aa2f2`)},
          mrkdwn: true
        )

      ClimateControl.modify(CI_PIPELINE_URL: 'https://test.net/gitlab-org/release/tools/-/pipelines/123') do
        without_dry_run do
          tag_notifier.execute
        end
      end
    end
  end
end

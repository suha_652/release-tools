# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::CNGImage do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .to_s'
  it_behaves_like 'project .security_group', 'gitlab-org/security/charts/components'
  it_behaves_like 'project .security_path', 'gitlab-org/security/charts/components/images'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-org/build/CNG' }
  end

  describe '.dev_path' do
    it { expect(described_class.dev_path).to eq 'gitlab/charts/components/images' }
  end

  describe '.group' do
    it { expect(described_class.group).to eq 'gitlab-org/build' }
  end

  describe '.dev_group' do
    it { expect(described_class.dev_group).to eq 'gitlab/charts/components' }
  end

  describe '.metadata_project_name' do
    it { expect(described_class.metadata_project_name).to eq 'cng-ee' }
  end
end

# frozen_string_literal: true

# Helpers for working with Rollback classes
module RollbackHelper
  include MetadataHelper

  def stub_product_version(gitlab_sha, omnibus_sha)
    product_version = build(:product_version)
    metadata = build_metadata(gitlab_sha: gitlab_sha, omnibus_sha: omnibus_sha)

    allow(product_version)
      .to receive(:metadata)
      .and_return(metadata)

    product_version
  end

  def rollback_stub_comparison(overrides = {})
    target = overrides.delete(:target) || build(:product_version)
    current = overrides.delete(:current) || build(:product_version)

    defaults = {
      timeout?: false,
      empty?: false,
      post_deploy_migrations: [],
      web_url: 'https://example.com/',
      current_rails_sha: SecureRandom.hex(6)[0, 11],
      safe?: true
    }

    ReleaseTools::Rollback::Comparison.new(current: current, target: target).tap do |instance|
      defaults.merge(overrides).each do |k, v|
        allow(instance).to receive(k).and_return(v)
      end
    end
  end

  def rollback_upcoming_deployment_stub(overrides = {})
    defaults = {
      any?: false,
      stale_cleanup: nil
    }

    instance_double('ReleaseTools::Rollback::UpcomingDeployment', defaults.merge(overrides))
  end
end

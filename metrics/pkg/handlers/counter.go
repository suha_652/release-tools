package handlers

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
)

type counter struct {
	metric metrics.Counter
}

func NewCounter(metric metrics.Counter) Pluggable {
	return &counter{metric}
}

func (c *counter) PlugRoutes(r *mux.Router) {
	subRouter := r.PathPrefix(route(c.metric)).Subrouter()

	subRouter.HandleFunc("/inc", c.incHandlerFunc)
}

func (c *counter) incHandlerFunc(w http.ResponseWriter, r *http.Request) {
	labels := getLabels(r)
	if err := c.metric.CheckLabels(labels); err != nil {
		badRequest(w, r, err.Error())

		return
	}

	c.metric.Inc(labels...)

	answer(w, r, "Incremented")
}

package handlers

import (
	"net/http"
	"testing"
)

type mockCounter struct {
	mockMetric
	cnt float64
}

func (m *mockCounter) Inc(labels ...string) {
	m.cnt += 1
}

func TestCounterVecHandler(t *testing.T) {
	meta := &mockCounter{}
	meta.expectedLabels = []string{"green"}
	handler := NewCounter(meta).(*counter)

	req, err := meta.apiRequest("inc", "")
	if err != nil {
		t.Fatal(err)
	}

	rr := testRequest(req, handler)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if meta.cnt != 1 {
		t.Errorf("Metrics not incremented: expected 1 got %v", meta.cnt)
	}
}

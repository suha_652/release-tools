package metrics

import (
	"fmt"
)

type Metadata interface {
	Namespace() string
	Name() string
	Subsystem() string

	CheckLabels(values []string) error
}

type MetricOption func(*descriptionOpts)

func WithName(name string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.name = name
	}
}

func WithSubsystem(subsystem string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.subsystem = subsystem
	}
}

func WithHelp(help string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.help = help
	}
}

// WithLabel defines a new label for the metrics with a list of allowed values.
// NOTE: when multiple labels are defined, it is important to not alter the order of
//  invocation of the WithLabel functions, the API interface for labels is positional
//  and we should not change the order
func WithLabel(name string, values []string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.labels = append(desc.labels, name)
		desc.labelValues = append(desc.labelValues, values)
	}
}

// WithLabelReset initializes the label set provided as parameter.
// Vector metrics are not published until a first read or write operation happens for a given label set,
// this funcition will force a read on the metric initializing the label set to 0
func WithLabelReset(labels ...string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.labelsToInitialize = append(desc.labelsToInitialize, labels)
	}
}

// WithCartesianProductLabelReset initializes the provided label set with an n-th cartesian product of the allowed values.
// NOTE: it must be invoked after all the WithLabel initializers!
func WithCartesianProductLabelReset() MetricOption {
	return func(desc *descriptionOpts) {
		if len(desc.labelValues) == 0 {
			return
		}

		// index is a slice containing the current permutation of label values indexes.
		//  index[i] will be the current index for desc.labelValues[i],
		//  on each step of the loop we will produce something like this:
		//  given n = len(desc.labelValues)-1
		//   { labelValues[0][index[0]], ... labelValues[i][index[i]], .... labelValues[n][index[n]] }
		index := make([]int, len(desc.labelValues))

		// nextIndex sets index to the next permutation of labels, the index is built incrementing
		//  elements from the end of the slice.
		// Each element goes from the first to the last label value index.
		// This function will increment index values such that for each i>0, 0 <= index[i] < lens(i).
		//
		// Example:
		// - assuming we have 3 labels with 2, 2, and 3 values respectivery,
		// - and index = { 0, 0, 0 }
		//
		// nextIndex will update index with the following values on each invocation:
		// - { 0, 0, 1 }
		// - { 0, 0, 2 }
		// - { 0, 1, 0 }
		// - { 0, 1, 1 }
		// - { 0, 1, 2 }
		// - { 1, 0, 0 }
		// - { 1, 0, 1 }
		// - { 1, 0, 2 }
		// - { 1, 1, 0 }
		// - { 1, 1, 1 }
		// - { 1, 1, 2 }
		// - { 2, 0, 0 } <-- this is not a valid index, we will use this as the exit condition for our initializer
		nextIndex := func() {
			// we loop over the labels from the last one to the first one, increasing each index (and returning)
			//  until we reach the last valid index for that label.
			// We the last index is found, we reset to 0 and move to the next label.
			for labelIdx := len(index) - 1; labelIdx >= 0; labelIdx-- {
				index[labelIdx]++

				// we don't want to go to the next label until we processed all the possible label values for the current labelIdx.
				// here is where we identify if the current values in index are a valid next index and return
				if index[labelIdx] < len(desc.labelValues[labelIdx]) || // the current index[labelIdx] is valid
					labelIdx == 0 { // or we are handling the last index (we loop in backward)

					// here index contains a new permutation
					return
				}

				// here we have already built all the permutations for labelIdx
				// it's time to set it back to 0 and let the loop move to the next label
				index[labelIdx] = 0
			}
		}

		// we loop on every permutation from nextIndex until we reach an invalid index
		for firsLabelSize := len(desc.labelValues[0]); index[0] < firsLabelSize; nextIndex() {
			var labels []string

			for labelIdx, labelValueIdx := range index {
				labels = append(labels, desc.labelValues[labelIdx][labelValueIdx])
			}

			WithLabelReset(labels...)(desc)
		}
	}
}

func WithBuckets(buckets []float64) MetricOption {
	return func(desc *descriptionOpts) {
		desc.buckets = buckets
	}
}

func applyMetricOptions(opts []MetricOption) *descriptionOpts {
	desc := &descriptionOpts{
		description: &description{
			labels:      make([]string, 0),
			labelValues: make([][]string, 0),
		},
		labelsToInitialize: make([][]string, 0),
	}

	for _, applyOpt := range opts {
		applyOpt(desc)
	}

	return desc
}

type description struct {
	name      string
	subsystem string

	labels      []string
	labelValues [][]string
}

type descriptionOpts struct {
	*description

	help string

	labelsToInitialize [][]string

	buckets []float64
}

func (d *description) Namespace() string { return "delivery" }
func (d *description) Name() string      { return d.name }
func (d *description) Subsystem() string { return d.subsystem }

// CheckLabels verifies user provided labels for cardinality and allowed values
// - values is a slice with user provided label values
//
// values has the same order of m.labels and m.labelValues, hence values[i] is the value for
// m.labels[i] and it must be included in m.labelValues[i]
func (m *description) CheckLabels(values []string) error {
	expectedLabels := len(m.labels)
	if len(values) != expectedLabels {
		return fmt.Errorf("Expected %d labels", expectedLabels)
	}

	for i, labelValue := range values {
		found := false

		for _, allowedValue := range m.labelValues[i] {
			if labelValue == allowedValue {
				found = true
				break
			}
		}

		if !found {
			return fmt.Errorf("%q is not a valid %q value", labelValue, m.labels[i])
		}
	}

	return nil
}
